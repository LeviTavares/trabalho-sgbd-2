package controller;

import gerenciador.Gerenciador;
import view.MainView;



public class MainController {
	
	static MainView mainView;
	static Gerenciador gerenciador;
	static int contador;
	
	public static void main (String[] args)
	{
		mainView = new MainView();
		gerenciador = new Gerenciador();
		contador = 1;
		boolean b;
		int sel;
		int op = 0;
		
		while(op != 8)
		{
			mainView.mostrarGrafo(gerenciador);
			op = mostrarMenu();
			
			switch (op) {
				case 1:
					String nome = "TR" + contador;
					gerenciador.trBegin(nome);
					contador++;
					break;
				case 2:
					b = gerenciador.trRead();
					if(!b)
					{
						mainView.mostrarInfo();
					}
					break;
				case 3:
					b = gerenciador.trWrite();
					if(!b)
					{
						mainView.mostrarInfo();
					}
					break;
				case 4:
					b = gerenciador.trTerminate();
					if(!b)
					{
						mainView.mostrarInfo();
					}
					break;
				case 5:
					sel = mainView.getKeyRollback();
					b = gerenciador.trRollback(sel);
					if(!b)
					{
						mainView.mostrarInfo();
					}
					break;
				case 6:
					b = gerenciador.trCommit();
					if(!b)
					{
						mainView.mostrarInfo();
					}
					break;
				case 7:
					sel = mainView.getKeyFinish();
					b = gerenciador.trFinish(sel);
					if(!b)
					{
						mainView.mostrarInfo();
					}
					break;
			}
		}
	}
	
	static int mostrarMenu()
	{
		return mainView.mostrarMenuPrincipal();
	}

}
