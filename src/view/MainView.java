package view;

import gerenciador.Gerenciador;

import java.util.ArrayList;
import java.util.Scanner;

import model.Transacao;


public class MainView {
	
	public int mostrarMenuPrincipal(){
		int op;
		Scanner leitor = new Scanner(System.in);

		System.out.println("|--------------------------------------------------|");
		System.out.println("|----Sistema de Gerenciamento de Banco de Dados----|");
		System.out.println("|--------------------------------------------------|");
		System.out.println("|------------Selecione a opção desejada------------|");
		System.out.println("|--------------------------------------------------|");
		System.out.println("|1 - TR_Begin -------------------------------------|");
		System.out.println("|2 - Read -----------------------------------------|");
		System.out.println("|3 - Write ----------------------------------------|");
		System.out.println("|4 - TR_Terminate ---------------------------------|");
		System.out.println("|5 - TR_Rollback ----------------------------------|");
		System.out.println("|6 - TR_Commit ------------------------------------|");
		System.out.println("|7 - TR_Finish ------------------------------------|");
		System.out.println("|--------------------------------------------------|");
		System.out.println("|8 - Sair -----------------------------------------|");
		System.out.println("|--------------------------------------------------|");
		System.out.println();
		
		op = leitor.nextInt();		
		
		
		return op;
	}
	
	public void mostrarGrafo(Gerenciador gerenciador){
		ArrayList<Transacao> temp;
		
		System.out.println("|--------------------------------------------------|");
		System.out.println("|----------------Grafo de Transações---------------|");
		System.out.println("|--------------------------------------------------|");
		System.out.println("|--------------------------------------------------|");
		System.out.println("|------------------Nó TR_Iniciada------------------|");
		System.out.println("|--------------------------------------------------|");
		temp = gerenciador.getTrIniciada();
		if(temp.isEmpty())
		{
			System.out.println("|-----------------------Vazio----------------------|");
		}
		else
		{
			for(Transacao t : temp)
			{
				System.out.println("|-- " + t.getNome());
			}
		}
		System.out.println("|--------------------------------------------------|");
		
		//Ativa
		System.out.println("|--------------------------------------------------|");
		System.out.println("|---------------------Nó Ativa---------------------|");
		System.out.println("|--------------------------------------------------|");
		temp = gerenciador.getAtiva();
		if(temp.isEmpty())
		{
			System.out.println("|-----------------------Vazio----------------------|");
		}
		else
		{
			for(Transacao t : temp)
			{
				System.out.println("|-- " + t.getNome());
			}
		}
		System.out.println("|--------------------------------------------------|");
		
		//Processo Cancelamento
		System.out.println("|--------------------------------------------------|");
		System.out.println("|-------------Nó Processo_Cancelamento-------------|");
		System.out.println("|--------------------------------------------------|");
		temp = gerenciador.getProcessoCancelamento();
		if(temp.isEmpty())
		{
			System.out.println("|-----------------------Vazio----------------------|");
		}
		else
		{
			for(Transacao t : temp)
			{
				System.out.println("|-- " + t.getNome());
			}
		}
		System.out.println("|--------------------------------------------------|");
		
		//Processo Efetivação
		System.out.println("|--------------------------------------------------|");
		System.out.println("|--------------Nó Processo_Efetivação--------------|");
		System.out.println("|--------------------------------------------------|");
		temp = gerenciador.getProcessoEfetivacao();
		if(temp.isEmpty())
		{
			System.out.println("|-----------------------Vazio----------------------|");
		}
		else
		{
			for(Transacao t : temp)
			{
				System.out.println("|-- " + t.getNome());
			}
		}
		System.out.println("|--------------------------------------------------|");
		
		//Efetivada
		System.out.println("|--------------------------------------------------|");
		System.out.println("|-------------------Nó Efetivada-------------------|");
		System.out.println("|--------------------------------------------------|");
		temp = gerenciador.getEfetivada();
		if(temp.isEmpty())
		{
			System.out.println("|-----------------------Vazio----------------------|");
		}
		else
		{
			for(Transacao t : temp)
			{
				System.out.println("|-- " + t.getNome());
			}
		}
		System.out.println("|--------------------------------------------------|");
		
		//Tr_Finalizada
		System.out.println("|--------------------------------------------------|");
		System.out.println("|-----------------Nó TR_Finalizada-----------------|");
		System.out.println("|--------------------------------------------------|");
		temp = gerenciador.getTrFinalizada();
		if(temp.isEmpty())
		{
			System.out.println("|-----------------------Vazio----------------------|");
		}
		else
		{
			for(Transacao t : temp)
			{
				System.out.println("|-- " + t.getNome());
			}
		}
		System.out.println("|--------------------------------------------------|");
		
		System.out.println();
		
		
	}
	
	public void mostrarInfo()
	{	
		System.out.println("|--------------------------------------------------|");
		System.out.println("|---Não foi possível mudar o estado da transação---|");
		System.out.println("|--------------------------------------------------|");
		System.out.println();
	}
	
	public int getKeyRollback(){
		int chave;
		Scanner leitor = new Scanner(System.in);
		
		System.out.println("|--------------------------------------------------|");
		System.out.println("|Qual o estado da transação que deve ser modificada|");
		System.out.println("|--------------------------------------------------|");
		System.out.println("|1 - Ativa ----------------------------------------|");
		System.out.println("|2 - Processo_Efetivação --------------------------|");
		System.out.println("|--------------------------------------------------|");
		System.out.println();

		chave = leitor.nextInt();
		
		return chave;
	}
	
	public int getKeyFinish(){
		int chave;
		Scanner leitor = new Scanner(System.in);
		
		System.out.println("|--------------------------------------------------|");
		System.out.println("|Qual o estado da transação que deve ser modificada|");
		System.out.println("|--------------------------------------------------|");
		System.out.println("|1 - Processo_Cancelamento ------------------------|");
		System.out.println("|2 - Efetivada ------------------------------------|");
		System.out.println("|--------------------------------------------------|");
		System.out.println();

		chave = leitor.nextInt();
		
		return chave;
	}

}
