package gerenciador;

import java.util.ArrayList;

import model.Transacao;

public class Gerenciador {
	
	private ArrayList<Transacao> trIniciada;
	private ArrayList<Transacao> ativa;
	private ArrayList<Transacao> processoCancelamento;
	private ArrayList<Transacao> processoEfetivacao;
	private ArrayList<Transacao> efetivada;
	private ArrayList<Transacao> trFinalizada;
	
	public Gerenciador() {
		trIniciada = new ArrayList<Transacao>();
		ativa = new ArrayList<Transacao>();
		processoCancelamento = new ArrayList<Transacao>();
		processoEfetivacao = new ArrayList<Transacao>();
		efetivada = new ArrayList<Transacao>();
		trFinalizada = new ArrayList<Transacao>();
	}
	
	public void trBegin(String nome)
	{
		trIniciada.add(new Transacao(nome));
	}
	
	public boolean trRead()
	{
		if (trIniciada.isEmpty())
		{
			return false;
		}
		else
		{
			ativa.add(trIniciada.remove(0));
			return true;
		}
	}
	
	public boolean trWrite()
	{
		return trRead();
	}
	
	public boolean trTerminate()
	{
		if (ativa.isEmpty())
		{
			return false;
		}
		else
		{
			processoEfetivacao.add(ativa.remove(0));
			return true;
		}
	}
	
	public boolean trRollback(int op)
	{
		//Tira do Ativa
		if (op == 1)
		{
			if (ativa.isEmpty())
			{
				return false;
			}
			else
			{
				processoCancelamento.add(ativa.remove(0));
				return true;
			}
		}
		//Tira do processoEfetivacao
		else
		{
			if (processoEfetivacao.isEmpty())
			{
				return false;
			}
			else
			{
				processoCancelamento.add(processoEfetivacao.remove(0));
				return true;
			}
		}
	}
	
	public boolean trCommit()
	{
		if (processoEfetivacao.isEmpty())
		{
			return false;
		}
		else
		{
			efetivada.add(processoEfetivacao.remove(0));
			return true;
		}
	}
	
	public boolean trFinish(int op)
	{
		//Tira do processoCancelamento
		if (op == 1)
		{
			if (processoCancelamento.isEmpty())
			{
				return false;
			}
			else
			{
				trFinalizada.add(processoCancelamento.remove(0));
				return true;
			}
		}
		//Tira do Efetivada
		else
		{
			if (efetivada.isEmpty())
			{
				return false;
			}
			else
			{
				trFinalizada.add(efetivada.remove(0));
				return true;
			}
		}
	}

	public ArrayList<Transacao> getTrIniciada() {
		return trIniciada;
	}

	public ArrayList<Transacao> getAtiva() {
		return ativa;
	}

	public ArrayList<Transacao> getProcessoCancelamento() {
		return processoCancelamento;
	}

	public ArrayList<Transacao> getProcessoEfetivacao() {
		return processoEfetivacao;
	}

	public ArrayList<Transacao> getEfetivada() {
		return efetivada;
	}

	public ArrayList<Transacao> getTrFinalizada() {
		return trFinalizada;
	}
	
	
	
	
}

